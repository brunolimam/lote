require "./player"
require "connections/game_connection"
require "connections/chat_connection"
require "./piece"
require "./board_ui"

require "byebug"

class Board
	attr_accessor :game_connection, :chat_connection, :player_one, :player_two, :board_ui, :my_turn, :paused, :surrendered, :reset, :second_player_want_reset, :player_one_lose, :player_two_lose

	@@max_pieces = 12

	def self.max_pieces
    @@max_pieces
  end

	def initialize game_connection, chat_connection, player_one, player_two, who_start
		@game_connection = game_connection
		@chat_connection = chat_connection
		@player_one = player_one
		@player_two = player_two

		@who_start = who_start
		@my_turn = check_who_start
		@paused = false
		@surrender = false
		@reset = false
		@player_one_lose = false
		@player_two_lose = false

		self.class.create_app self, player_one, player_two
		self.listen_message
		self.wait_move
	end

	def check_who_start
		return @who_start == @player_one ? true : false
	end

	def self.create_app board, player_one, player_two
		init_board_ui board, player_one, player_two
	end

	def listen_message
		Thread.new {
			loop {
				@chat_connection.receive_message do |message|
					board_ui.append_message player_two, message
				end
			}
		}.run
	end

	def send_message message
		Thread.new { @chat_connection.send_message message }.run
	end

	def click_on_position x, y
		return if !@my_turn || @paused || @surrendered || @player_one_lose || @player_two_lose
		@second_player_want_reset = false

		#verifica peca selecionada pra mover
		if @piece_selected.nil?

			#pega na posicao
			@piece_selected = select_piece x, y
			
			if !@piece_selected.nil? 
				
				#verifica se a peca eh do jogador corrente
				if @piece_selected.color == player_one.color

					#seleciona graficamente peca
					@piece_selected.toggle_select
				else
					#desleciona peca inimiga selecionada e retorna
					unselect_piece
				end
			else
				#retorna se nao tiver mais pecas para criar
				return if !remains_pieces_to_create?

				#cria peca
				create_piece x,y	
				@my_turn = false	
			end

		elsif unselect_piece? x, y
			piece_unselected = true
			@piece_selected.toggle_select
			unselect_piece
		else
			#analisa movimento
			return if !can_move?(x, y)

			@my_turn = false

			#move peca selecionada
			@piece_selected.update_position x, y
			unselect_piece
		end

		redraw_game

  	if @piece_selected.nil? && !piece_unselected
			transmit_game
  	end

	end

	def remains_pieces_to_create?
		return @player_one.pieces.size < self.class.max_pieces ? true : false
	end

	def unselect_piece? x, y
		return @piece_selected == select_piece(x, y) ? true : false
	end

	def unselect_piece
		@piece_selected = nil
	end

	def select_piece x, y
		players = [player_one, player_two]

		players.each do |player|
			piece = player.pieces.select{|piece| piece.x == x && piece.y == y }.first
			return piece if !piece.nil?
		end

		nil
	end

	def create_piece x, y
		player_one.add_piece x, y if player_one.pieces.size < self.class.max_pieces
	end

	def can_move? x, y
		#EM CIMA DE OUTRA PECA
		piece = select_piece x,y
		return false if !piece.nil?

		#MOVIMENTO NAO PERMITIDO
		offset_x = (@piece_selected.x-x).abs
		offset_y = (@piece_selected.y-y).abs

		#VERIFICANDO SE TEM PECA ENINMIGA NO CAMINHO
		enemy_piece = nil

		if @piece_selected.x == x && offset_y == 2
			y_to_check = (@piece_selected.y+y)/2
			enemy_piece = select_piece x, y_to_check
		elsif @piece_selected.y == y && offset_x == 2
			x_to_check = (@piece_selected.x+x)/2
			enemy_piece = select_piece x_to_check, y
		end

		if !enemy_piece.nil? && enemy_piece.color != player_one.color
			enemy_piece.kill
		end

		true
	end

	def wait_move
		Thread.new {
			loop {
				@game_connection.wait_opponent do |player_one, player_two, my_turn, paused, surrendered, reset|

					@player_one.pieces = player_one.pieces
					@player_two.pieces = player_two.pieces
					@my_turn = my_turn
					@paused = paused
					@surrendered = surrendered

					analyze_reset reset
					redraw_game
				end	
			}
		}.run
	end

	def analyze_reset reset
		if @reset && reset 
			reset_game
		elsif reset
			@second_player_want_reset = reset
		elsif @second_player_want_reset && reset == false
			erase_variables
			@second_player_want_reset = reset
			@reset = reset
		else
			@second_player_want_reset = reset
			@reset = reset
		end
	end

	def pause_game
		@paused = !@paused
		redraw_game

		transmit_game
	end

	def surrender_game
		@surrendered = true
		board_ui.show_alert "Voce desistiu"

		transmit_game
	end

	def reset_game
		if @reset
			erase_variables
		else
			@reset = true
		end

		transmit_game
	end

	def redraw_game
		board_ui.draw_game player_one, player_two
	end

	def transmit_game
		Thread.new { @game_connection.send_players @player_one, @player_two, !@my_turn, @paused, @surrendered, @reset }.run
	end

	private
	def erase_variables
		@player_one.pieces = []
		@player_two.pieces = []
		@my_turn = check_who_start
		@reset = false
		@surrendered = false
		@paused = false
		@player_one_lose = false
		@player_two_lose = false
		redraw_game
	end
end

# player_one = Player.new("Bruno", "Vermelho")
# Board.new(GameConnection.new(nil), ChatConnection.new(nil), player_one, Player.new("Alsi", "Verde"), player_one)
