
def init_board_ui board, player_one, player_two

	board.board_ui = Shoes.app height: 500, width: 800, resizable: false, title: "#{player_one.name} - #{player_one.color}" do
	  background rgb(240, 250, 208)

	  @board = board
	  @positions = []
	  @messages = []
	  

	  def draw_game player_one, player_two
	  	clear

		  #LATERAL DIREITA
		  stack left: 600, top: 0, height: 500, width: 200 do
	     	border black, strokewidth: 3

	     	#STATUS DO JOGO
	     	stack height: 50, margin: 10, margin_bottom: 0 do

	     		status = @board.paused ? "Pausado" : "Vez de: #{@board.my_turn ? player_one.name : player_two.name }"

	     		if @board.surrendered
	     			status = "Parabens, voce ganhou" 
	     		end

	     		if @board.player_one_lose && !@board.player_two_lose
	     			status = "Voce perdeu"
	     		elsif @board.player_two_lose && !@board.player_one_lose
	     			status = "Voce ganhou"
	     		elsif @board.player_one_lose && @board.player_two_lose
	     			status = "Empate"
	     		end	

	     		if @board.second_player_want_reset
	     			status = "#{player_two.name} deseja reinciar partida" 
	     		end

	     		@game_status = inscription status, align: "center"
	     	end

	     	#JOGADORES
	     	stack height: 50, margin_left: 10, margin_right: 10 do 
	     		inscription "#{player_one.name}: #{player_one.remaining_pieces_quantity} pecas", background: red
	     		inscription "#{player_two.name}: #{player_two.remaining_pieces_quantity} pecas", margin_top: 0
	     	end


	     	# PAUSAR E DESISTIR
			 	stack height: 80 do 
		     	button "Pausar", width: 200, top: 5 do
		     		@board.pause_game
				 	end

				 	button "Desistir", width: 200, top: 30 do
						@board.surrender_game 
				 	end

				 	button "Reiniciar", width: 200, top: 55 do
						@board.reset_game
				 	end
			 	end

			 	#CHAT
			 	stack do

		 			#titulo superior
		     	flow height: 25 do
			     	border black, strokewidth: 3

		     		para "CHAT", align: "center"
				 	end

				 	# MENSAGENS
				 	@messages_stack = stack height: 225, width: 200, margin: 10, scroll: true do
				 		@messages.each do |message|
							para message
				 		end
				 	end

				 	# TEXT FIELD E BOTAO ENVIAR MENSAGEM
				 	stack margin: 10 do
					 	@message_edit_line = edit_line width: 185

					 	flow do 
							@send_message_button = button "Enviar", left: 101 do
								message = @message_edit_line.text
								@message_edit_line.text = ""

								@board.send_message message
								append_message player_one, message
						 	end
					 	end
				 	end
			 	end

		  end

	  	draw_board player_one, player_two
	  end

	  def draw_board player_one, player_two
	    @positions = []

	  	# TABULEIRO
	  	5.times do |i|
		    6.times do |j|

		      position = stack height: 100, width: 100, left: j*100, top: i*100  do
	          value = (i+j).even? ? 0 : 255
	          background rgb(value, value, value)

	          click do
	          	@board.click_on_position i,j
	          end
		      end

		      @positions << position
		    end
		  end

		  draw_pieces player_one, player_two
	  end

	  def update_status status
	  	@game_status.text = status
	  end

	  #PECAS
	  def draw_pieces player_one, player_two
	  	players = [player_one, player_two]
	  	players.each do |player|

	  		player.remaining_pieces.each do |piece|
	  			i = piece.x
	  			j = piece.y

	  			position = @positions[i*6+j]

	  			position.clear do
	          value = (i+j).even? ? 0 : 255
	      		background rgb(value, value, value)

	      		color = piece.color == "Azul" ? "201a7e" : green
	      		color = gray if piece.selected

	        	oval radius: 40, top: 10, left: 10, fill: color
	      	end
		  	end

	  	end
	  end

	  def append_message player, message
	  	label_message = "#{player.name}: #{message}"
	  	@messages << label_message

			@messages_stack.app do
				@messages_stack.append do
					para label_message
				end

				@messages_stack.scroll_top = @messages_stack.scroll_max 
			end
		end

		def show_alert message
			self.alert message
		end

		def set_surrender_button_title title
			@surrender_button.app do
				@surrender_button.text = title
			end
		end

		draw_game player_one, player_two
	end
end