require "connections/connection"

class ChatConnection < Connection

	PORT = 3002

	def initialize socket
		super socket
	end

	def self.start_server
		self.new(super PORT)
	end

	def self.connect_server ip_address
		self.new(super ip_address, PORT)
	end
	
	def send_message message
		@socket.puts({message: message}.to_json)
	end

	def receive_message
		json_received = JSON.parse(@socket.gets)

		if !json_received["message"].nil?
			yield json_received["message"]
		end	
	end
end