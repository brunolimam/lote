require 'socket'
require 'json'
require "./player"

class Connection

	@@size = 999999

	attr_accessor :socket

	def initialize socket
		@socket = socket
	end

	def self.start_server port
		server = TCPServer.new("0.0.0.0", port) # Server bind to port 2000
	  server.accept    # Wait for a client to connect
	end

	def self.connect_server ip_address, port
		TCPSocket.new ip_address, port
	end

	def close_socket
		@socket.close
	end
end