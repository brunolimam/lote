require "connections/connection"

class GameConnection < Connection

	PORT = 3001

	def initialize socket
		super socket
	end

	def self.start_server
		self.new(super PORT)
	end

	def self.connect_server ip_address
		self.new(super ip_address, PORT)
	end

	def get_server_player player
		player_hash = JSON.parse(@socket.gets)

		@socket.puts player.to_hash.to_json
		
		Player.from_hash(player_hash)
	end

	def get_client_player player
		@socket.puts player.to_hash.to_json

		Player.from_hash(JSON.parse(@socket.gets))
	end

	def send_players player_one, player_two, change_turn, paused, surrender, reset
		@socket.puts({player_one: player_one.to_hash, player_two: player_two.to_hash, change_turn: change_turn, paused: paused, surrender: surrender, reset: reset}.to_json)
	end

	def wait_opponent
		json_received = JSON.parse(@socket.gets)

		if !json_received["player_one"].nil? && !json_received["player_two"].nil?

			player_two = Player.from_hash(json_received["player_one"])
			player_one = Player.from_hash(json_received["player_two"])

			yield player_one, player_two, json_received["change_turn"], json_received["paused"], json_received["surrender"], json_received["reset"]
		end
	end

end