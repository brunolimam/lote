require "./board"
require "./player"

Shoes.app height: 200, width: 300, resizable: false, title: "Yote" do
  background rgb(240, 250, 208)

  @stack = stack margin: 10 do

  	flow do
  		para "Nome"
		  @player_name = edit_line margin_left: 10, width: 135
		end

	  flow do
	  	ip_address = Socket.ip_address_list.detect{|intf| intf.ipv4_private?}.ip_address

		  @address_ip_label = inscription "Endereco IP: #{ip_address}"

		  @init_server_button = button "Iniciar", left: 180, width: 100 do

		  	@status_conection.text = "Esperando oponente conectar"
		  	connect_server_toggle false

		  	Thread.new {
				  @game_connection = GameConnection.start_server

			  	show_status_connection

				  @player_one = Player.new(@player_name.text, "Azul")
				  @player_two = @game_connection.get_client_player @player_one

			  	start_game @player_one
				}.run

				Thread.new {
				  @chat_connection = ChatConnection.start_server

				  start_game @player_one
				}.run
		  	
		  end
		end

		flow do
		  @server_address_input = edit_line width: 180
		  @server_address_input.text = "0.0.0.0"

		  @connect_server_button = button "Conectar", width: 100 do
		  	@status_conection.text = "Conectando-se ao oponente (#{@server_address_input.text})"
		  	create_server_toggle false

		  	Thread.new {

			  	@game_connection = GameConnection.connect_server @server_address_input.text
			  	@chat_connection = ChatConnection.connect_server @server_address_input.text

			  	show_status_connection

					@player_one = Player.new(@player_name.text, "Verde")
				  @player_two = @game_connection.get_server_player @player_one

			  	start_game @player_two
				}.run

		  end
		end

		flow do
			@cancel_button = button "Cancelar", left: 180, width: 100 do
				# @game_connection.close_socket if !@game_connection.nil?

		    @status_conection.text = ""

		  	connect_server_toggle true
		  	create_server_toggle true
		  end
		end

		@status_conection = para ""
	end


	def show_status_connection		
		if !@game_connection.socket.nil?
  		@status_conection.text = "Conectado com o servidor"
  	else
  		@status_conection.text = "Falha na conexao"
  	end
	end

	def connect_server_toggle show
		if show
			@connect_server_button.show
			@server_address_input.show
		else
			@connect_server_button.hide
			@server_address_input.hide
		end
	end

	def create_server_toggle show
		if show
			@init_server_button.show
			@address_ip_label.show
		else
			@init_server_button.hide
			@address_ip_label.hide		
		end
	end

	def start_game who_start
		return if @player_one.nil? || @player_two.nil? || @chat_connection.nil? || @game_connection.nil?

		board = Board.new(@game_connection, @chat_connection, @player_one, @player_two, who_start)
		@stack.app.close
	end
end
