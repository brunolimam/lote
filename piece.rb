class Piece
	attr_accessor :player, :color, :x, :y, :selected, :dead

	def initialize color, x = nil, y = nil, dead = false
		@player = player
		@color = color
		@x = x
		@y = y
		@selected = false
		@dead = dead
	end

	def self.from_hash hash
    self.new(hash["color"], hash["x"], hash["y"], hash["dead"])
	end

	def to_json
		{color: color, x: x, y: y, dead: dead}
	end

	def update_position x, y
		@x = x
		@y = y
		@selected = false
	end

	def toggle_select
		@selected = !@selected
	end

	def kill
		@x=nil
		@y=nil
		@dead = true
	end
end