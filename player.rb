require "./piece"
require 'json'
require "byebug"

class Player
	attr_accessor :name, :pieces, :color

	def initialize name, color, pieces = nil
		@name = name
		@color = color
		@pieces = pieces.nil? ? [] : pieces.map { |piece_hash| Piece.from_hash(piece_hash) }
	end

	def self.from_hash hash
    self.new(hash["name"], hash["color"], hash["pieces"])
	end

	def to_hash
		{name: name, color: color, pieces: pieces.map(&:to_json)}
	end

	def remaining_pieces
		pieces.select{ |piece| piece.dead == false }
	end

	def remaining_pieces_quantity
		Board.max_pieces-(pieces.size-remaining_pieces.size)
	end
	
	def add_piece x, y
		pieces << Piece.new("#{color}", x, y)
	end
end